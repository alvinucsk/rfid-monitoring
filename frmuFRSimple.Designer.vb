﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmuFRSimple
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.mnuMeni = New System.Windows.Forms.MenuStrip()
        Me.mnuExitItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.pnlReader = New System.Windows.Forms.Panel()
        Me.clockTime = New System.Windows.Forms.Label()
        Me.lblClockStatus = New System.Windows.Forms.Label()
        Me.lbltime = New System.Windows.Forms.Label()
        Me.departmentName = New System.Windows.Forms.Label()
        Me.employeeName = New System.Windows.Forms.Label()
        Me.txtCardSerial = New System.Windows.Forms.TextBox()
        Me.txtCardUIDSize = New System.Windows.Forms.TextBox()
        Me.lblCardSerial = New System.Windows.Forms.Label()
        Me.txtCardType = New System.Windows.Forms.TextBox()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtReaderType = New System.Windows.Forms.TextBox()
        Me.lblReaderSerial = New System.Windows.Forms.Label()
        Me.txtReaderSerial = New System.Windows.Forms.TextBox()
        Me.pnlReader_conn = New System.Windows.Forms.ToolStripStatusLabel()
        Me.pnlReader_error_code = New System.Windows.Forms.ToolStripStatusLabel()
        Me.pnlReader_error_expl = New System.Windows.Forms.ToolStripStatusLabel()
        Me.stbReader = New System.Windows.Forms.StatusStrip()
        Me.positionName = New System.Windows.Forms.Label()
        Me.mnuMeni.SuspendLayout()
        Me.pnlReader.SuspendLayout()
        Me.stbReader.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuMeni
        '
        Me.mnuMeni.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExitItem})
        Me.mnuMeni.Location = New System.Drawing.Point(0, 0)
        Me.mnuMeni.Name = "mnuMeni"
        Me.mnuMeni.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.mnuMeni.Size = New System.Drawing.Size(1204, 24)
        Me.mnuMeni.TabIndex = 0
        Me.mnuMeni.Text = "MenuStrip1"
        '
        'mnuExitItem
        '
        Me.mnuExitItem.Name = "mnuExitItem"
        Me.mnuExitItem.Size = New System.Drawing.Size(37, 20)
        Me.mnuExitItem.Text = "Exit"
        '
        'pnlReader
        '
        Me.pnlReader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReader.Controls.Add(Me.positionName)
        Me.pnlReader.Controls.Add(Me.clockTime)
        Me.pnlReader.Controls.Add(Me.lblClockStatus)
        Me.pnlReader.Controls.Add(Me.lbltime)
        Me.pnlReader.Controls.Add(Me.departmentName)
        Me.pnlReader.Controls.Add(Me.employeeName)
        Me.pnlReader.Controls.Add(Me.txtCardSerial)
        Me.pnlReader.Controls.Add(Me.txtCardUIDSize)
        Me.pnlReader.Controls.Add(Me.stbReader)
        Me.pnlReader.Controls.Add(Me.lblCardSerial)
        Me.pnlReader.Controls.Add(Me.txtCardType)
        Me.pnlReader.Controls.Add(Me.txtReaderSerial)
        Me.pnlReader.Controls.Add(Me.lblReaderSerial)
        Me.pnlReader.Controls.Add(Me.txtReaderType)
        Me.pnlReader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlReader.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlReader.Location = New System.Drawing.Point(0, 24)
        Me.pnlReader.Name = "pnlReader"
        Me.pnlReader.Size = New System.Drawing.Size(1204, 493)
        Me.pnlReader.TabIndex = 3
        '
        'clockTime
        '
        Me.clockTime.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clockTime.AutoSize = True
        Me.clockTime.Font = New System.Drawing.Font("Verdana", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clockTime.Location = New System.Drawing.Point(3, 312)
        Me.clockTime.Name = "clockTime"
        Me.clockTime.Size = New System.Drawing.Size(366, 59)
        Me.clockTime.TabIndex = 25
        Me.clockTime.Text = "Clock IN TIME"
        '
        'lblClockStatus
        '
        Me.lblClockStatus.AutoSize = True
        Me.lblClockStatus.Font = New System.Drawing.Font("Verdana", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClockStatus.Location = New System.Drawing.Point(19, 266)
        Me.lblClockStatus.Name = "lblClockStatus"
        Me.lblClockStatus.Size = New System.Drawing.Size(187, 32)
        Me.lblClockStatus.TabIndex = 24
        Me.lblClockStatus.Text = "CLOCKED IN"
        '
        'lbltime
        '
        Me.lbltime.AutoSize = True
        Me.lbltime.Font = New System.Drawing.Font("Verdana", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltime.Location = New System.Drawing.Point(11, 45)
        Me.lbltime.Name = "lbltime"
        Me.lbltime.Size = New System.Drawing.Size(168, 29)
        Me.lbltime.TabIndex = 23
        Me.lbltime.Text = "System Time"
        '
        'departmentName
        '
        Me.departmentName.AutoSize = True
        Me.departmentName.Font = New System.Drawing.Font("Verdana", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.departmentName.Location = New System.Drawing.Point(19, 220)
        Me.departmentName.Name = "departmentName"
        Me.departmentName.Size = New System.Drawing.Size(307, 32)
        Me.departmentName.TabIndex = 22
        Me.departmentName.Text = "WELCOME EMPLOYEE"
        '
        'employeeName
        '
        Me.employeeName.AutoSize = True
        Me.employeeName.Font = New System.Drawing.Font("Verdana", 50.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.employeeName.Location = New System.Drawing.Point(11, 90)
        Me.employeeName.Name = "employeeName"
        Me.employeeName.Size = New System.Drawing.Size(582, 80)
        Me.employeeName.TabIndex = 21
        Me.employeeName.Text = "Juan De La Cruz"
        '
        'txtCardSerial
        '
        Me.txtCardSerial.BackColor = System.Drawing.Color.White
        Me.txtCardSerial.Enabled = False
        Me.txtCardSerial.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCardSerial.Location = New System.Drawing.Point(134, 9)
        Me.txtCardSerial.Name = "txtCardSerial"
        Me.txtCardSerial.ReadOnly = True
        Me.txtCardSerial.Size = New System.Drawing.Size(160, 21)
        Me.txtCardSerial.TabIndex = 20
        Me.txtCardSerial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCardUIDSize
        '
        Me.txtCardUIDSize.BackColor = System.Drawing.Color.White
        Me.txtCardUIDSize.Enabled = False
        Me.txtCardUIDSize.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCardUIDSize.Location = New System.Drawing.Point(93, 9)
        Me.txtCardUIDSize.Name = "txtCardUIDSize"
        Me.txtCardUIDSize.ReadOnly = True
        Me.txtCardUIDSize.Size = New System.Drawing.Size(35, 21)
        Me.txtCardUIDSize.TabIndex = 18
        Me.txtCardUIDSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me.txtCardUIDSize, " ")
        '
        'lblCardSerial
        '
        Me.lblCardSerial.AutoSize = True
        Me.lblCardSerial.Location = New System.Drawing.Point(11, 12)
        Me.lblCardSerial.Name = "lblCardSerial"
        Me.lblCardSerial.Size = New System.Drawing.Size(37, 13)
        Me.lblCardSerial.TabIndex = 6
        Me.lblCardSerial.Text = "Card"
        '
        'txtCardType
        '
        Me.txtCardType.BackColor = System.Drawing.Color.White
        Me.txtCardType.Enabled = False
        Me.txtCardType.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCardType.Location = New System.Drawing.Point(52, 9)
        Me.txtCardType.Name = "txtCardType"
        Me.txtCardType.ReadOnly = True
        Me.txtCardType.Size = New System.Drawing.Size(35, 21)
        Me.txtCardType.TabIndex = 5
        Me.txtCardType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Timer
        '
        Me.Timer.Enabled = True
        Me.Timer.Interval = 500
        '
        'txtReaderType
        '
        Me.txtReaderType.BackColor = System.Drawing.Color.White
        Me.txtReaderType.Enabled = False
        Me.txtReaderType.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReaderType.Location = New System.Drawing.Point(111, 511)
        Me.txtReaderType.Name = "txtReaderType"
        Me.txtReaderType.ReadOnly = True
        Me.txtReaderType.Size = New System.Drawing.Size(108, 21)
        Me.txtReaderType.TabIndex = 1
        Me.txtReaderType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblReaderSerial
        '
        Me.lblReaderSerial.AutoSize = True
        Me.lblReaderSerial.Location = New System.Drawing.Point(3, 512)
        Me.lblReaderSerial.Name = "lblReaderSerial"
        Me.lblReaderSerial.Size = New System.Drawing.Size(102, 13)
        Me.lblReaderSerial.TabIndex = 2
        Me.lblReaderSerial.Text = "Reader Details"
        '
        'txtReaderSerial
        '
        Me.txtReaderSerial.BackColor = System.Drawing.Color.White
        Me.txtReaderSerial.Enabled = False
        Me.txtReaderSerial.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReaderSerial.Location = New System.Drawing.Point(225, 511)
        Me.txtReaderSerial.Name = "txtReaderSerial"
        Me.txtReaderSerial.ReadOnly = True
        Me.txtReaderSerial.Size = New System.Drawing.Size(108, 21)
        Me.txtReaderSerial.TabIndex = 3
        Me.txtReaderSerial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pnlReader_conn
        '
        Me.pnlReader_conn.AutoSize = False
        Me.pnlReader_conn.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right
        Me.pnlReader_conn.Name = "pnlReader_conn"
        Me.pnlReader_conn.Size = New System.Drawing.Size(120, 17)
        Me.pnlReader_conn.Text = " "
        '
        'pnlReader_error_code
        '
        Me.pnlReader_error_code.AutoSize = False
        Me.pnlReader_error_code.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right
        Me.pnlReader_error_code.Name = "pnlReader_error_code"
        Me.pnlReader_error_code.Size = New System.Drawing.Size(50, 17)
        Me.pnlReader_error_code.Text = " "
        '
        'pnlReader_error_expl
        '
        Me.pnlReader_error_expl.AutoSize = False
        Me.pnlReader_error_expl.Name = "pnlReader_error_expl"
        Me.pnlReader_error_expl.Size = New System.Drawing.Size(1017, 17)
        Me.pnlReader_error_expl.Spring = True
        Me.pnlReader_error_expl.Text = " "
        '
        'stbReader
        '
        Me.stbReader.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stbReader.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.pnlReader_conn, Me.pnlReader_error_code, Me.pnlReader_error_expl})
        Me.stbReader.Location = New System.Drawing.Point(0, 469)
        Me.stbReader.Name = "stbReader"
        Me.stbReader.Size = New System.Drawing.Size(1202, 22)
        Me.stbReader.SizingGrip = False
        Me.stbReader.TabIndex = 16
        Me.stbReader.Text = " "
        '
        'positionName
        '
        Me.positionName.AutoSize = True
        Me.positionName.Font = New System.Drawing.Font("Verdana", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.positionName.Location = New System.Drawing.Point(19, 179)
        Me.positionName.Name = "positionName"
        Me.positionName.Size = New System.Drawing.Size(150, 32)
        Me.positionName.TabIndex = 26
        Me.positionName.Text = "POSITION"
        '
        'frmuFRSimple
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1204, 483)
        Me.Controls.Add(Me.pnlReader)
        Me.Controls.Add(Me.mnuMeni)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MainMenuStrip = Me.mnuMeni
        Me.MaximizeBox = False
        Me.Name = "frmuFRSimple"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "RFID EMPLOYEE ID SYSTEM"
        Me.mnuMeni.ResumeLayout(False)
        Me.mnuMeni.PerformLayout()
        Me.pnlReader.ResumeLayout(False)
        Me.pnlReader.PerformLayout()
        Me.stbReader.ResumeLayout(False)
        Me.stbReader.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuMeni As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuExitItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlReader As System.Windows.Forms.Panel
    Friend WithEvents lblCardSerial As System.Windows.Forms.Label
    Friend WithEvents txtCardType As System.Windows.Forms.TextBox
    Friend WithEvents Timer As System.Windows.Forms.Timer
    Friend WithEvents txtCardUIDSize As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents txtCardSerial As System.Windows.Forms.TextBox
    Friend WithEvents departmentName As Label
    Friend WithEvents employeeName As Label
    Friend WithEvents lbltime As Label
    Friend WithEvents lblClockStatus As Label
    Friend WithEvents clockTime As Label
    Friend WithEvents stbReader As StatusStrip
    Friend WithEvents pnlReader_conn As ToolStripStatusLabel
    Friend WithEvents pnlReader_error_code As ToolStripStatusLabel
    Friend WithEvents pnlReader_error_expl As ToolStripStatusLabel
    Friend WithEvents txtReaderSerial As TextBox
    Friend WithEvents lblReaderSerial As Label
    Friend WithEvents txtReaderType As TextBox
    Friend WithEvents positionName As Label
End Class
