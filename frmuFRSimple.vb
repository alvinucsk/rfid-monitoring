﻿

Imports uFR_Coder_Simple.uFCoder
Imports MySql.Data.MySqlClient

Public Class frmuFRSimple
    Dim ERROR_CODES(200) As String
    Dim bDLCardType As Byte
    Dim conn As MySqlConnection
    Dim command As MySqlCommand
    Dim query As String
    Dim reader As MySqlDataReader

    Private boLoopStart As Boolean = False,
            boFunctionOn = False,
            boCONN = False

    Private Property FunctionStart() As Boolean
        Get
            Return boFunctionOn
        End Get
        Set(value As Boolean)
            boFunctionOn = value
        End Set
    End Property

    Private Property LoopStart() As Boolean
        Get
            Return boLoopStart
        End Get
        Set(value As Boolean)
            boLoopStart = value
        End Set
    End Property

    Private Sub FullRangeERC()
        Dim iValue As Integer
        For Each iValue In [Enum].GetValues(GetType(ERRORCODES))
            ERROR_CODES(iValue) = [Enum].GetName(GetType(ERRORCODES), iValue)
        Next
    End Sub

    Private Sub mnuExitItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExitItem.Click
        Application.Exit()
    End Sub

    Private Sub frmUFRCoderSimple_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        clockTime.Text = ""
        lblClockStatus.Text = ""
        departmentName.Text = ""
        employeeName.Text = ""
        positionName.Text = ""

        ' Connect Mysql
        conn = New MySqlConnection
        conn.ConnectionString = "server=localhost;userid=root;database=dtr-monitoring"


        Try
            conn.Open()
            MessageBox.Show("Conneted To Server")

        Catch ex As MySqlException
            MessageBox.Show(ex.Message)
        End Try

        FullRangeERC()
    End Sub

    Private Sub MainThread()
        Dim ulReaderType, ulReaderSerial As ULong
        Dim iRResult, iCResult As ULong
        Dim bCardUIDSize, bCardType As Byte
        Dim baCardUID(9) As Byte
        Dim sBuffer As String = ""
        LoopStart = True
        If Not boCONN Then
            iRResult = ReaderOpen()
            If iRResult = DL_OK Then
                boCONN = True
                pnlReader_conn.Text = "CONNECTED"
            Else
                pnlReader_conn.Text = "NOT CONNECTED"
                txtReaderType.Clear()
                txtReaderSerial.Clear()
                txtCardType.Clear()
                txtCardSerial.Clear()
                txtCardUIDSize.Clear()
            End If
        End If

        If boCONN Then
            iRResult = GetReaderType(ulReaderType)
            If iRResult = DL_OK Then
                txtReaderType.Text = ulReaderType.ToString("X")
                iRResult = GetReaderSerialNumber(ulReaderSerial)
                If iRResult = DL_OK Then
                    txtReaderSerial.Text = ulReaderSerial.ToString("X")
                    iCResult = GetDlogicCardType(bDLCardType)
                    If iCResult = DL_OK Then


                        GetCardIdEx(bCardType, baCardUID(0), bCardUIDSize)
                        For bBr = 0 To bCardUIDSize - 1
                            sBuffer += baCardUID(bBr).ToString("X2")
                        Next
                        txtCardType.Text = bDLCardType.ToString("X2")
                        txtCardUIDSize.Text = bCardUIDSize.ToString("X2")

                        txtCardSerial.Text = sBuffer
                        Dim idNumber = txtCardType.Text + "-" + txtCardUIDSize.Text + "-" + txtCardSerial.Text
                        clockTime.Text = lbltime.Text
                        query = "select employees.*,
                            departments.name as department_name, 
                            (select action from daily_time_in_out where employee_id=employees.ID order by daily_time_in_out.ID DESC limit 1) as action
                            from employees
                            join
                            departments on employees.department = departments.ID where employees.id_num='" + idNumber + "'"
                        command = New MySqlCommand(query, conn)

                        reader = command.ExecuteReader

                        Dim count As Integer = 0

                        While reader.Read
                            count = count + 1
                            departmentName.Text = reader.GetString(8) + " Department"
                            employeeName.Text = reader.GetString(1) + " " + reader.GetString(2) + " " + reader.GetString(3)
                            positionName.Text = reader.GetString(5)

                            If Not reader.IsDBNull(9) Then
                                If String.Compare(reader.GetString(9), "in") = 0 Then
                                    lblClockStatus.Text = "TIMED OUT"
                                    query = "INSERT INTO `daily_time_in_out` (`employee_id`, `action`) VALUES (" + reader.GetString(0) + ", 'out')"
                                Else
                                    lblClockStatus.Text = "TIMED IN"
                                    query = "INSERT INTO `daily_time_in_out` (`employee_id`, `action`) VALUES (" + reader.GetString(0) + ", 'in')"
                                End If
                            Else
                                lblClockStatus.Text = "TIMED IN"
                            End If

                            Console.WriteLine("{0} {1} {2}", reader.GetString(1), reader.GetString(2), reader.GetString(3))
                            Console.WriteLine(query)


                        End While
                        reader.Close()
                        ' Update Database with Login
                        command = New MySqlCommand(query, conn)
                        reader = command.ExecuteReader
                        reader.Close()
                    Else
                        txtCardType.Clear()
                        txtCardSerial.Clear()
                        txtCardUIDSize.Clear()
                    End If
                End If
            Else
                ReaderClose()
                boCONN = False
            End If
        End If
        LoopStart = False
    End Sub

    Private Sub frmuFRSimple_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        conn.Close()
    End Sub

    Private Sub Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer.Tick
        If Not FunctionStart Then
            lbltime.Text = Format(Now, "MM/dd/yyyy hh:mm:ss")
            MainThread()
        End If
    End Sub

    Private Sub btnLinearWrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If LoopStart Or FunctionStart Then Return
        Try
            FunctionStart = True
            Dim usBytesRet As UShort = 0
            Dim usLinearRead As UShort = 0
            Dim usDataLength As UShort = 0
            Dim usLinearAddress As UShort = 0


        Catch ex As System.FormatException
            MessageBox.Show(CONVERT_ERROR, "Error !", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            FunctionStart = False
        End Try
    End Sub
End Class
