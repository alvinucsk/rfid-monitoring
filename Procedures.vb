﻿
'*********************************************************************************

'   Program                :  uFr Simple
'   File                   :  Procedures.vb
'   Description            :  Other functions
'   Manufacturer           :  D-Logic
'   Development enviroment :  Microsoft VB.NET 2012 Express framework version 4.5
'   Revisions			   :  
'   Version                :  2.0

'**********************************************************************************/

Imports System.Text.RegularExpressions
Module Procedures
    'card type     
    Public Const MIFARE_CLASSIC_1k As Byte = &H8,
                 MIFARE_CLASSIC_4k         = &H18

    'authenticate
    Public Const MIFARE_AUTHENT1A As Byte = &H60,
                 MIFARE_AUTHENT1B         = &H61

    Public Const DL_OK As Byte            = &H0,
                 KEY_INDEX                = &H0



    'for reader                    
    Public Const FRES_OK_LIGHT As Byte    = 4,
                 FRES_OK_SOUND            = 0,
                 FERR_LIGHT               = 2,
                 FERR_SOUND               = 0

    ' sectors and blocks
    Public Const MAX_SECTORS_1k As Byte = &H10,
                 MAX_SECTORS_4k         = &H28

    Public Const CONVERT_ERROR As String      = "You must enter a number between 0 and 255 or 0 and FF hexadecimal !"
    Public Const MAX_VALUE_ENTER As String    = "You must enter a number between 0 and 255 !"
    Public Const APPROPRIATE_FORMAT As String = "You must enter the appropriate format !"

    Public Const NEW_CARD_KEY_A As String = "txtNewCardKeyA"
    Public Const NEW_CARD_KEY_B As String = "txtNewCardKeyB"
    Public Const NEW_READER_KEY As String = "txtNewReaderKey"

    


    Public Function MaxBytes(bTypeOfCard As Byte) As Integer
        Dim shMaxBytes As Integer = 0
        Select Case bTypeOfCard
            Case &H8
                shMaxBytes = 752
            Case &H18
                shMaxBytes = 3440
        End Select
        Return shMaxBytes
    End Function

    Public Function MaxBlock(bTypeOfCard As Byte) As Integer
        Dim iResult As Integer = 0

        If bTypeOfCard = MIFARE_CLASSIC_1k Then
            iResult = MAX_SECTORS_1k * 4
        Else
            If bTypeOfCard = MIFARE_CLASSIC_4k Then
                iResult = ((MAX_SECTORS_1k * 2) * 4) + ((MAX_SECTORS_1k - 8) * 16)
            End If
        End If
        Return iResult
    End Function
    


  

    ' Convert dec to hex and vice versa of text box
    Public Sub CheckHexBox(ByVal chkCheckBox As CheckBox, ByVal sKeyName As String, ByVal pnlContainer As Panel)        
        For Each CtrlKey As Control In pnlContainer.Controls
            If CtrlKey.Name = sKeyName Then
                If String.IsNullOrEmpty(CtrlKey.Text.Trim) Then
                    Continue For
                Else
                    If chkCheckBox.Checked Then
                        CtrlKey.Text = System.Convert.ToByte(CtrlKey.Text.Trim()).ToString("X")
                    Else
                        CtrlKey.Text = System.Convert.ToByte(CtrlKey.Text, 16).ToString()
                    End If

                End If
            End If
        Next
        
    End Sub
    Public Sub DecHexConversion(ByVal chkCheckBox As CheckBox, ByVal sKeyName As String, ByVal pnlContainer As Panel, ByVal baDimKey() As Byte)
        Dim bCount As Byte = 0
        If chkCheckBox.Checked Then
            For Each CtrlKey As Control In pnlContainer.Controls
                If CtrlKey.Name = sKeyName Then
                    baDimKey(bCount) = Convert.ToByte(CtrlKey.Text, 16)
                    bCount += 1
                End If
            Next
        Else
            For Each CtrlKey As Control In pnlContainer.Controls
                If CtrlKey.Name = sKeyName Then
                    baDimKey(bCount) = Convert.ToByte(CtrlKey.Text)
                    bCount += 1
                End If
            Next
        End If
    End Sub



    Public Function WriteArray(ByVal bWriteData As Array, ByVal iDataLength As Integer, ByVal iMaxBytes As Integer) As Byte()

        Dim bCloneArray(iMaxBytes) As Byte

        Dim br As Integer
        Array.Copy(bWriteData, bCloneArray, bWriteData.Length)
        For br = bWriteData.Length To iMaxBytes
            bCloneArray(br) = 32
        Next
        Return bCloneArray
    End Function


End Module
