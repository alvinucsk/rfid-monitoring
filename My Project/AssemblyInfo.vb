﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("uFR Simple")> 
<Assembly: AssemblyDescription("Functions to work with the keys card and reader key.Suport for 4k")> 
<Assembly: AssemblyCompany("D-Logic")> 
<Assembly: AssemblyProduct("uFR Simple")> 
<Assembly: AssemblyCopyright("")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("af634986-925e-428e-8aa5-2a39248327bb")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.0")> 
<Assembly: AssemblyFileVersion("2.0.0.0")> 
